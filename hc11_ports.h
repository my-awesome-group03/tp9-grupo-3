 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   hc11_ports.h
 * Author: scamara
 *
 * Created on May 14, 2022, 5:52 PM
 */
#include <stdint.h>

#ifndef HC11_PORTS_H
#define HC11_PORTS_H

void bitSet (char port, uint8_t bit_to_modify);
void maskOn (char port, uint16_t bit_to_modify);
void bitClr (char port, uint8_t bit_to_modify);
void bitToggle (char port, uint8_t value);
void maskToggle (char port, uint16_t value);
uint8_t bitGet (char port, uint8_t bit_to_get);
void maskOff (char port, uint16_t value);

#endif //HC11_PORTS_H
