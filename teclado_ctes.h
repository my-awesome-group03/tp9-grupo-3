/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   teclado_ctes.h
 * Author: scamara
 *
 * Created on May 15, 2022, 7:30 PM
 */

#ifndef TECLADO_CTES_H
#define TECLADO_CTES_H

#define MIN '0'
#define MAX '7'
#define BYTE 8

#define ERR -1          //Símbolo de error para análisis de valores por teclado.
#define OPUESTO 8     
#define APAGAR 9
#define PRENDER 10
#define FIN 11

#endif //TECLADO_CTES_H

