#include <stdio.h>
#include <stdint.h>

#include "hc11_ports.h"
#include "teclado.h"
#include "teclado_ctes.h"

/********************************************************************************************
*                                    ARCHIVO PRINCIPAL                                      *
********************************************************************************************/

/********************************************************************************************
*                                                                                           *
*       El archivo a continuación contiene un programa que simula 8 LEDS conectados al      *
*       puerto A del HC11. Esto lo hace utilizando principalmente la librería hc11_ports    *
*       que contine variadas funciones que permiten modificar y observar el estado de       *
*       las luces de los LEDS conectados al puerto A.                                       *
*                                                                                           *
*       Funcionamiento:                                                                     *
*                                                                                           *
*           Este programa hace uso de dos funciones principales. Por un lado, para          *
*       saber qué fue ingresado por línea de comandos, llama a la función teclado que       *
*       toma lo ingresado por línea de comandos y verifica la validez de lo ingresado       *
*       y, de serlo, guarda el valor correspondiente. De ser incorrecto el valor, loopea    *
*       hasta que se ingrese un valor válido y poder actuar en función a éste. De ser       *
*       válido, puede realizar las siguientes funciones:                                    *
*                                                                                           *
*           - Opuesto ('t'): todos los LEDS cambian a su estado opuesto (de estar           *
*       encendidos se apagan y de estar apagados se encienden.                              *
*           - Apagar ('c'): apaga todos los LEDS.                                           *
*           - Encender ('s'): prende todos los LEDS.                                        *
*           - Encender una luz ('0'-'7'): enciende el bit especificado mediante             *
*       numeración del 0 al 7 de los bits de derecha a izquierda.                           *
*                                                                                           *
*       El programa se mantiene en funcionamiento hasta que se indica la finalización       *
*       de su ejecución ingresando 'q' en la la línea de comandos.                          *
*                                                                                           *
*       Se consideran valores inválidos todos aquellos no listados en la previa|            *
*       aclaración o ingresar más de un caracter por línea de comandos.                     *
*                                                                                           *
********************************************************************************************/

#define PUERTO 'A' //Se especifica el puerto a modificar (puede ser A o B).

static void printPort(void); //Función que imprime estado del puerto en pantalla.
static void limpbuffer(void); //Función que limpia el buffer.

int main (void)
{
    int tecla;
    
    while ((tecla = teclado()) != FIN)
    {
        int i;

        while (tecla == ERR && tecla != FIN)
        {
            printf ("Caracter inválido o se ha ingresado más de uno. Por favor ingrese un valor válido.\n");
            limpbuffer();
            tecla = teclado ();
        }

        if (tecla != FIN)
        {
            if (tecla == OPUESTO)               //Usa la función bitToggle para cambiar bit a bit por su opuesto de cada uno del puerto especificado.
            {
                for ( i=0 ; i<BYTE ; i++ )
                {
                    bitToggle(PUERTO, i);
                }
            }
        
            else if (tecla == APAGAR)           //Usa la función bitClr para cambiar bit a bit por 0 de cada uno del puerto especificado.
            {
                for (i = 0; i<BYTE; i++)
                {
                    bitClr(PUERTO, i);
                }
            }

            else if (tecla == PRENDER)          //Usa la función bitSet para cambiar bit a bit por 1 de cada uno del puerto especificado.
            {
                for (i = 0; i<BYTE; i++)
                {
                    bitSet (PUERTO, i);
                }
            }

            else                                //Usa la función bitSet para cambiar el bit especificado por 1 del puerto especificado.
            {
                bitSet (PUERTO, tecla);
            }
            
            printPort();                       //Sólo de haberse ingresado un valor válido imprime una representación del puerto en pantalla.
            
        }
        
    }

    return 0;
    
}

/*

FUNCIÓN PRINT PORT:

   Descripción: Imprime representación de LEDS conectados a un puerto de ocho bits. Imprime en el siguiente formato:
   
   Puerto x:
     _ _ _ _ _ _ _ _
    | |*|*|*|*|*|*|*|
     ¯ ¯ ¯ ¯ ¯ ¯ ¯ ¯
     7 6 5 4 3 2 1 0

   Entrada: ---
   Salida: ---
 
*/

static void printPort (void)
{

    int i;
    char max = MAX;

    printf ("\nPuerto %c:\n", PUERTO);

    for ( i=0 ; i<BYTE ; i++ )
    {
        printf(" _");
    }

    printf ("\n");

    for ( i=BYTE-1 ; i>=0 ; i-- )       
    {
        printf("|");
        
        if ( bitGet (PUERTO, i) )       //Por cada bit prendido, imprime un asterisco
        {
            printf ("*");
        }
        else                            //De lo contrario, imprime un espacio
        {
            printf (" ");
        }
    }

    printf ("|\n");

    for ( i=0 ; i<BYTE ; i++ )
    {
        printf(" ¯");
    }

    printf ("\n");

    for ( i=0 ; i<BYTE ; i++ )
    {
        printf(" %c", max--);
    }

    printf ("\n\n");

}

/*

FUNCIÓN LIMPBUFFER:

Descripción: Limpia el getchar para evitar complicaciones con lo ingresado por teclado al pedir reingreso.
Recibe: ---
Devuelve: ---

*/

static void limpbuffer (void)          //Esta funcion limpia el Buffer
{
	int c;
    
	do
        {
            c = getchar();
        }
        while (c != '\n');
}