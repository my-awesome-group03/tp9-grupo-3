A := gcc
OPTIONS := -g -Wall

OBJS := TP9_G3.o hc11_ports.o teclado.o

#Targets

TP9_G3: ${OBJS}
	${A} ${OBJS} ${OPTIONS} -o TP9_G3

TP9_G3.o: TP9_G3.c hc11_ports.h teclado.h teclado_ctes.h
	${A} TP9_G3.c -c ${OPTIONS}

hc11_ports.o: hc11_ports.c hc11_ports.h 
	${A} hc11_ports.c -c ${OPTIONS}

teclado.o: teclado.c teclado.h teclado_ctes.h
	${A} teclado.c -c ${OPTIONS}

#Borra objetos

clean:
	rm *.o
