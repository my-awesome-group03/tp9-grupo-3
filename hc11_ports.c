/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <stdint.h>
#include "hc11_ports.h"

#define MASK(num)  ( 1 << (num) )   //macro que posiciona un "1" en el bit especificado
#define OPPOSITE(num, mask)  ( (num) ^ (mask) ) //macro que realiza un xor con los bits de un puerto y una mascara
#define SETBIT(num, mask)  ( (num) | (mask) )   //macro que prende los bits de un puerto que se le ingresan con una mascara
#define CLRBIT(num, mask)  ( (num) & (mask) )   //macro que apaga los bits de un puerto que se le ingresan con una mascara
#define TOUPPER(x) (((x) >= 'a') && ((x) <= 'z') ? ((x) + 'A' - 'a') : (x)) //macro que coloca las letras en mayusucla


typedef struct
{
	uint8_t b0	:1 ;
	uint8_t b1	:1 ;
	uint8_t b2	:1 ;
	uint8_t b3	:1 ;
	uint8_t b4	:1 ;
	uint8_t b5	:1 ;
	uint8_t b6	:1 ;
	uint8_t b7	:1 ;
}port8bit_t;


typedef struct
{
	uint16_t b0	:1 ;
	uint16_t b1	:1 ;
	uint16_t b2	:1 ;
	uint16_t b3	:1 ;
	uint16_t b4	:1 ;
	uint16_t b5	:1 ;
	uint16_t b6	:1 ;
	uint16_t b7	:1 ;
	uint16_t b8	:1 ;
	uint16_t b9	:1 ;
	uint16_t b10	:1 ;
	uint16_t b11	:1 ;
	uint16_t b12	:1 ;
	uint16_t b13	:1 ;
	uint16_t b14	:1 ;
	uint16_t b15	:1 ;
}port16bit_t;



typedef struct
{
    port8bit_t  high; //b0-b7
    port8bit_t  low; //b0-b7
}portbyte_t;


typedef struct
{
    uint8_t  b;
    uint8_t  a;
}port_t;


typedef union
{
    port16bit_t	bit;  //b0-b15
    portbyte_t  byte; //low-high
    port_t      port; //a-b
    uint16_t    portd;

}portdata_t; //union que permite ingresar al valor del puerto D

static portdata_t hc11; //declaración de la union hc11

/*

FUNCIÓN bitSet:

Descripción: coloca el bit especificado del puerto ingresado en 1. Si el puerto o el bit son incorrectos, imprime un error
Recibe: un puerto (a, A, b, B, d, D) y el bit que se quiere modificar
Devuelve: ---

*/

void bitSet (char puerto, uint8_t value)
{
        if(puerto == 'a' || puerto == 'A')
        {
            if(value >=0 && value <=7)
            {
                hc11.port.a=SETBIT(hc11.port.a, MASK(value));
            }
            else
            {
                printf("numero de bit invalido \n");
            }
        }
        else if (puerto == 'b' || puerto == 'B')
        {
            if(value >=0 && value <=7)
            {
                hc11.port.b=SETBIT(hc11.port.b, MASK(value));
            }
            else
            {
                printf("numero de bit invalido \n");
            }
        }
        else if (puerto == 'd' || puerto == 'D')
        {
            if(value >=0 && value <=15)
            {
                hc11.portd=SETBIT(hc11.portd, MASK(value));
            }
            else
            {
                printf("numero de bit invalido \n");
            }
        }
        else
        {
            printf("puerto invalido");
        }

}

/*

FUNCIÓN maskOn:

Descripción: coloca los bits especificados con una mascara del puerto ingresado en 1. Si el puerto o los bits son incorrectos, imprime un error
Recibe: un puerto (a, A, b, B, d, D) y los bits que se quieren modificar
Devuelve: ---

*/

void maskOn (char puerto, uint16_t value)
{
        if(puerto == 'a' || puerto == 'A')
        {
            if(value <= 0xff)
            {
                hc11.port.a=SETBIT(hc11.port.a, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else if (puerto == 'b' || puerto == 'B')
        {
            if(value <= 0xff)
            {
                hc11.port.b=SETBIT(hc11.port.b, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else if (puerto == 'd' || puerto == 'D')
        {
            if(value <=0xffff)
            {
                hc11.portd=SETBIT(hc11.portd, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else
        {
            printf("puerto invalido");
        }

} 

/*

FUNCIÓN bitClr:

Descripción: coloca el bit especificado del puerto ingresado en 0. Si el puerto o el bit son incorrectos, imprime un error
Recibe: un puerto (a, A, b, B, d, D) y el bit que se quiere modificar
Devuelve: ---

*/

void bitClr (char puerto, uint8_t value)
{
        if(puerto == 'a' || puerto == 'A')
        {
            if(value >=0 && value <=7)
            {
                hc11.port.a=CLRBIT(hc11.port.a, ~(MASK(value)));
            }
            else
            {
                printf("numero de bit invalido \n");
            }
        }
        else if (puerto == 'b' || puerto == 'B')
        {
            if(value >=0 && value <=7)
            {
                hc11.port.b=CLRBIT(hc11.port.b, ~(MASK(value)));
            }
            else
            {
                printf("numero de bit invalido \n");
            }
        }
        else if (puerto == 'd' || puerto == 'D')
        {
            if(value >=0 && value <=15)
            {
                hc11.portd=CLRBIT(hc11.portd, ~(MASK(value)));
            }
            else
            {
                printf("numero de bit invalido \n");
            }
        }
        else
        {
            printf("puerto invalido");
        }

}

/*

FUNCIÓN maskOff:

Descripción: coloca los bits especificados con una mascara del puerto ingresado en 0. Si el puerto o los bits son incorrectos, imprime un error
Recibe: un puerto (a, A, b, B, d, D) y los bits que se quieren modificar
Devuelve: ---

*/

void maskOff (char puerto, uint16_t value)
{
    TOUPPER(puerto);
        if(puerto == 'A')
        {
            if(value <= 0xff)
            {
                hc11.port.a=CLRBIT(hc11.port.a, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else if (puerto == 'B')
        {
            if(value <= 0xff)
            {
                hc11.port.b=CLRBIT(hc11.port.b, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else if (puerto == 'D')
        {
            if(value <=0xffff)
            {
                hc11.portd=CLRBIT(hc11.portd, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else
        {
            printf("puerto invalido");
        }
}

/*

FUNCIÓN bitToggle:

Descripción: invierte el bit especificado del puerto ingresado. Si el puerto o el bit son incorrectos, imprime un error
Recibe: un puerto (a, A, b, B, d, D) y el bit que se quiere modificar
Devuelve: ---

*/

void bitToggle (char puerto, uint8_t value)
{
        if(puerto == 'a' || puerto == 'A')
        {
            if(value >=0 && value <=7)
            {
                hc11.port.a=OPPOSITE(hc11.port.a, MASK(value));
            }
            else
            {
                printf("numero de bit invalido:");
            }
        }
        else if (puerto == 'b' || puerto == 'B')
        {
            if(value >=0 && value <=7)
            {
                hc11.port.b=OPPOSITE(hc11.port.b, MASK(value));
            }
            else
            {
                printf("numero de bit invalido:");
            }
        }
        else if (puerto == 'd' || puerto == 'D')
        {
            if(value >=0 && value <=15)
            {
                hc11.portd=OPPOSITE(hc11.portd, MASK(value));
            }
            else
            {
                printf("numero de bit invalido:");
            }
        }
        else
        {
            printf("puerto invalido");
        }
}

/*

FUNCIÓN bitGet:

Descripción: lee el bit especificado del puerto ingresado. Si el puerto o el bit son incorrectos, imprime un error
Recibe: un puerto (a, A, b, B, d, D) y el bit que se quiere leer
Devuelve: el valor del bit ingresado.

*/

uint8_t bitGet (char puerto, uint8_t value)
{
    uint8_t valorbit;
    TOUPPER(puerto);
    if(puerto == 'A')
    {
        if(value >=0 && value <=7)
        {
            valorbit = (hc11.port.a & MASK(value)) >> value;
        }
        else
        {
            printf("numero de bit invalido \n");
        }
    }
    else if (puerto == 'B')
    {
        if(value >=0 && value <=7)
        {
            valorbit = (hc11.port.a & MASK(value)) >> value;
        }
        else
        {
            printf("numero de bit invalido \n");
        }
    }
    else if (puerto == 'D')
    {
        if(value >=0 && value <=15)
        {
            valorbit = (hc11.port.a & MASK(value)) >> value;
        }
        else
        {
            printf("numero de bit invalido \n");
        }
    }
    else
    {
        printf("puerto invalido");
    }

    return valorbit;
}


/*

FUNCIÓN maskToggle:

Descripción: invierte los bits especificados con una mascara del puerto ingresado. Si el puerto o los bits son incorrectos, imprime un error
Recibe: un puerto (a, A, b, B, d, D) y los bits que se quieren modificar
Devuelve: ---

*/

void maskToggle (char puerto, uint16_t value)
{
        if(puerto == 'a' || puerto == 'A')
        {
            if(value <= 0xff)
            {
                hc11.port.a=OPPOSITE(hc11.port.a, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else if (puerto == 'b' || puerto == 'B')
        {
            if(value <= 0xff)
            {
                hc11.port.b=OPPOSITE(hc11.port.b, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else if (puerto == 'd' || puerto == 'D')
        {
            if(value <=0xffff)
            {
                hc11.portd=OPPOSITE(hc11.portd, value);
            }
            else
            {
                printf("numero de bit invalido");
            }
        }
        else
        {
            printf("puerto invalido");
        }

}
