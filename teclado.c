#include <stdio.h>
#include "teclado.h"
#include "teclado_ctes.h"

/*
FUNCION TECLADO:

    Entrada: ---
    Salida: Esta función devuelve un -1 si se ingresa un valor invalido por teclado, devuelve variados valores que representan
    distintas acciones al presionar diferentes teclas por teclado o devuelve el valor numerico ingresado.
    Funcionamiento: Toma todos los valores ingresados por teclado y los analiza uno por uno. Si son números los convierte,
    si es alguna de los caracteres especificados devuelve su valor correspondiente definido en el .h y si es cualquier otro
    caracter devuelve un ERR.

    Caracteres de 'acción' especificados:

        - 't'
        - 'c'
        - 's'
        - 'q'
*/

int teclado(void)
{
 	int c;
	int num = 0;
        int val = 0;

	while( (val >= 0) && ((c=getchar()) != '\n') )
	{
        val++;

    	if ((c >= MIN) && (c <= MAX))               
    	{
			num = c-'0';
		}
        else if (c == 't')         
        {
            num = OPUESTO;
        }
        else if (c == 'c')
        {
			num = APAGAR;                  
        }
        else if (c == 's')
        {
            num = PRENDER;
        }
        else if (c == 'q')
        {
            num = FIN;
        }
        else
        {
            val = ERR;
            num = ERR;
        }
	}

    if (val != 1)
    {
        num = ERR;
    }

 	return num;
}
